package learn.dicoding.temperatureconverter.ui.main

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import learn.dicoding.temperatureconverter.ui.main.components.ConverterApp
import learn.dicoding.temperatureconverter.ui.main.components.StatefulTemperatureInput
import learn.dicoding.temperatureconverter.ui.main.components.StatelessTemperatureInput
import learn.dicoding.temperatureconverter.ui.main.components.TwoWayConverter
import learn.dicoding.temperatureconverter.ui.theme.TemperatureConverterTheme
import learn.dicoding.temperatureconverter.utils.Converter

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            TemperatureConverterTheme {
                // A surface container using the 'background' color from the theme
                Surface(modifier = Modifier.fillMaxSize(), color = MaterialTheme.colorScheme.background) {
                    Column {
                        StatefulTemperatureInput()
                        ConverterApp()
                        TwoWayConverter()
                    }
                }
            }
        }
    }
}
