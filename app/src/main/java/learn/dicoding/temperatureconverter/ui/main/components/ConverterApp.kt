package learn.dicoding.temperatureconverter.ui.main.components

import androidx.compose.foundation.layout.Column
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import learn.dicoding.temperatureconverter.ui.theme.TemperatureConverterTheme
import learn.dicoding.temperatureconverter.utils.Converter

@Composable
fun ConverterApp(
    modifier: Modifier = Modifier
){
    var input by remember { mutableStateOf("") }
    var output by remember { mutableStateOf("") }
    Column(
        modifier = modifier
    ) {
        StatelessTemperatureInput(input, output, onValueChange = {value->
            input = value
            output = Converter.celsiusToFahrenheit(value)
        })
    }
}

@Preview(showBackground = true)
@Composable
private fun ConverterAppPreview(){
    TemperatureConverterTheme {
        ConverterApp()
    }
}
