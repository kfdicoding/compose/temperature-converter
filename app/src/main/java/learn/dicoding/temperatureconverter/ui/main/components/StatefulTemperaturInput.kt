package learn.dicoding.temperatureconverter.ui.main.components

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.text.BasicTextField
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.OutlinedTextField
import androidx.compose.material3.Text
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import learn.dicoding.temperatureconverter.R
import learn.dicoding.temperatureconverter.ui.theme.TemperatureConverterTheme
import learn.dicoding.temperatureconverter.utils.Converter


@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun StatefulTemperatureInput(
    modifier: Modifier = Modifier
){
    var input by remember { mutableStateOf("") }
    var output by remember { mutableStateOf("") }

    Column(
        modifier = modifier.padding(16.dp)
    ) {
        Text(
            text = stringResource(R.string.stateful_converter),
            style = MaterialTheme.typography.titleLarge
        )
        OutlinedTextField(
            value = input,
            label = {
                Text(stringResource(R.string.enter_celsius))
            },
            onValueChange = {value->
                input = value
                output = Converter.celsiusToFahrenheit(value)
            },
            keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Number)
        )
        Text(stringResource(R.string.temperature_fahrenheit, output))

    }
}

@Preview(showBackground = true)
@Composable
private fun StatefulInputPreview(){
    TemperatureConverterTheme {
        StatefulTemperatureInput()
    }
}
