package learn.dicoding.temperatureconverter.ui.main.components

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import learn.dicoding.temperatureconverter.R
import learn.dicoding.temperatureconverter.ui.components.GeneralTemperatureInput
import learn.dicoding.temperatureconverter.ui.components.Scale
import learn.dicoding.temperatureconverter.ui.theme.TemperatureConverterTheme
import learn.dicoding.temperatureconverter.utils.Converter

@Composable
fun TwoWayConverter(
    modifier: Modifier = Modifier
){
    var celsius by remember{ mutableStateOf("") }
    var fahrenheit by remember{ mutableStateOf("") }

    Column(
        modifier = modifier.padding(16.dp)
    ) {
        Text(
            text = stringResource(R.string.two_way_converter),
            style = MaterialTheme.typography.titleLarge
        )
        GeneralTemperatureInput(
            scale = Scale.CELSIUS,
            input = celsius,
            onValueChange = {value->
                celsius = value
                fahrenheit = Converter.celsiusToFahrenheit(value)
            }
        )
        GeneralTemperatureInput(
            scale = Scale.FAHRENHEIT,
            input = fahrenheit,
            onValueChange = {value->
                fahrenheit = value
                celsius = Converter.fahrenheitToCelsius(value)
            }
        )
    }
}

@Preview(showBackground = true)
@Composable
private fun TwoWayConverterPreview(){
    TemperatureConverterTheme {
        TwoWayConverter()
    }
}
