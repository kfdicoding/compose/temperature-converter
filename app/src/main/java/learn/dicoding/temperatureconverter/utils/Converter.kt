package learn.dicoding.temperatureconverter.utils

object Converter {

    fun celsiusToFahrenheit(celsius: String) = celsius.toDoubleOrNull()?.let {
        (it * 9 / 5) + 32
    }.toString()

    fun fahrenheitToCelsius(fahrenheit: String) = fahrenheit.toDoubleOrNull()?.let {
        (it - 32) *5 / 9
    }.toString()
}
